export let MENU_ITEM = [
  {
    path: 'index',
    title: 'Dashboard',
    icon: 'home'
  },
  {
    path: 'profile',
    title: 'About Darren',
    icon: 'user'
  },
  {
    path: 'editor',
    title: 'Contact Darren',
    icon: 'comments'
  }
];
